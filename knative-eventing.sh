#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/eventing-crds.yaml
kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/eventing-core.yaml

# The following command installs an implementation of Channel that runs in-memory. 
# This implementation is nice because it is simple and standalone, but it is unsuitable for production use cases.

kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/in-memory-channel.yaml

# The following command installs an implementation of Broker that utilizes Channels:

kubectl apply --filename https://github.com/knative/eventing/releases/download/v0.14.0/channel-broker.yaml

kubectl apply --filename config/config-br-defaults.yml

# kubectl get pods --namespace knative-eventing
EOF

