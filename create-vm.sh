#!/bin/sh
eval $(cat vm.config)

multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
	--cloud-init ./cloud-init.yaml

#IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

# Initialize K3s on node
echo "👋 Initialize 📦 K3s on ${vm_name}..."

multipass mount workspace ${vm_name}:workspace
multipass mount config ${vm_name}:config

multipass info ${vm_name}

#multipass exec ${vm_name} -- sudo -- sh -c "echo \"${IP} ${vm_domain}\" >> /etc/hosts"

# install k3s
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
#curl -sfL https://get.k3s.io | sh -
curl -sfL https://get.k3s.io | sh -s - --disable traefik
EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"

multipass exec ${vm_name} sudo cat /etc/rancher/k3s/k3s.yaml > config/k3s.yaml

sed -i '' "s/127.0.0.1/$IP/" config/k3s.yaml

# 🖐 use this file to exchange data between VM creation script
# use: eval $(cat ../registry/workspace/export.registry.config)
target="workspace/export.cluster.config"
echo "vm_name=\"${vm_name}\";" >> ${target}
echo "vm_domain=\"${vm_domain}\";" >> ${target}
echo "vm_ip=\"${IP}\";" >> ${target}
