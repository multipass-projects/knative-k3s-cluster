# Knative on K3S cluster

## How to

🚧 WIP

## K9S

Install K9S: [https://github.com/derailed/k9s](https://github.com/derailed/k9s)

## How to run K9S

```bash
export KUBECONFIG=$PWD/config/k3s.yaml
k9s --all-namespaces
```
